<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Profile;
use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $credentials = $request->only('email', 'password');
        $token = Auth::attempt($credentials);

        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized',
            ], 401);
        }

        $user = Auth::user();
        $profile = $user->profile;
        if ($profile) {
            unset($profile['id'], $profile['user_id'], $profile['is_show']);

            $address = $profile->address;
            if ($address) {
                unset($address['id'], $address['profile_id']);
            }
        }

        return response()->json([
            'status' => 'success',
            'message' =>  'Logged In',
            'data' => [
                'user' => $user,
                'token' => $token
            ]
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // User
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|confirmed|string|min:6',

            // Profile
            'photo' => 'nullable|file|image|max:5120',
            'nik' => 'required|min:16|max:16|unique:profiles',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'birth_place' => 'required|string',
            'birth_date' => 'required|date|string',
            'gender' => 'required|string|in:L,P',
            'religion' => 'required|string',
            'marital_status' => 'required|string',
            'no_bpjs' => 'nullable|string',
            'about_me' => 'nullable|string',

            // Address
            'address_details' => 'required|string',
            'rt' => 'required|string|max:3',
            'rw' => 'required|string|max:3',
            'village' => 'required|string', // Kelurahan
            'district' => 'required|string', // Kecamatan
            'city' => 'required|string',
            'province' => 'required|string',
            'postal_code' => 'required|string|max:6',
            'country' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $photo_name = '';
        try {
            if (@$request->hasFile('photo')) {
                $photo_name = "photo_" . strtolower(str_replace(' ', '-', $request->file('photo')->getClientOriginalName()));
                $request->file('photo')->storeAs('public/profile/'.$request->nik, $photo_name);
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $profile = Profile::create([
            'user_id' => $user->id,
            'photo' => $photo_name,
            'nik' => $request->nik,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'birth_place' => $request->birth_place,
            'birth_date' => $request->birth_date,
            'gender' => $request->gender,
            'religion' => $request->religion,
            'marital_status' => $request->marital_status,
            'no_bpjs' => $request->no_bpjs,
            'about_me' => $request->about_me,
        ]);

        Address::create([
            'profile_id' => $profile->id,
            'detail' => $request->address_details,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'village' => $request->village,
            'district' => $request->district,
            'city' => $request->city,
            'province' => $request->province,
            'postal_code' => $request->postal_code,
            'country' => $request->country,
        ]);

        $profile = $user->profile;
        if ($profile) {
            unset($profile['id'], $profile['user_id'], $profile['is_show']);

            $address = $profile->address;
            if ($address) {
                unset($address['id'], $address['profile_id']);
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'User created successfully',
            'data' => $user,
        ]);
    }

    public function updateProfile(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            // Profile
            'photo' => 'nullable|file|image|max:5120',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'birth_place' => 'required|string',
            'birth_date' => 'required|date|string',
            'gender' => 'required|string|in:L,P',
            'religion' => 'required|string',
            'marital_status' => 'required|string',
            'no_bpjs' => 'nullable|string',
            'about_me' => 'nullable|string',

            // Address
            'address_details' => 'required|string',
            'rt' => 'required|string|max:3',
            'rw' => 'required|string|max:3',
            'village' => 'required|string', // Kelurahan
            'district' => 'required|string', // Kecamatan
            'city' => 'required|string',
            'province' => 'required|string',
            'postal_code' => 'required|string|max:6',
            'country' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $user = User::find($id);

        if (empty($user)) {
            return response()->json([
                "status" => "error",
                "message" => "User not found",
            ], 400);
        }

        $photo_name = '';
        try {
            if (@$request->hasFile('photo')) {
                Storage::delete('public/profile/'.$user->profile->nik.'/'.$user->profile->photo);

                $photo_name = "photo_" . strtolower(str_replace(' ', '-', $request->file('photo')->getClientOriginalName()));
                $request->file('photo')->storeAs('public/profile/'.$user->profile->nik, $photo_name);
            } else {
                if ($user->profile) {
                    $photo_name = $user->profile->photo;
                }
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        $profile = Profile::updateOrCreate([
            'user_id' => $id,
        ],[
            'photo' => $photo_name,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'birth_place' => $request->birth_place,
            'birth_date' => $request->birth_date,
            'gender' => $request->gender,
            'religion' => $request->religion,
            'marital_status' => $request->marital_status,
            'no_bpjs' => $request->no_bpjs,
            'about_me' => $request->about_me,
        ]);

        Address::updateOrCreate([
            'profile_id' => $profile->id,
        ],[
            'detail' => $request->address_details,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'village' => $request->village,
            'district' => $request->district,
            'city' => $request->city,
            'province' => $request->province,
            'postal_code' => $request->postal_code,
            'country' => $request->country,
        ]);

        $profile = Profile::where('user_id', $id)->first();
        unset($profile['is_show']);

        $address = $profile->address;
        if ($address) {
            unset($address['id'], $address['profile_id']);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'User updated successfully',
            'data' => $profile,
        ]);
    }

    public function changePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string|min:6',
            'password' => 'required|confirmed|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $user = User::find($id);

        if (empty($user)) {
            return response()->json([
                "status" => "error",
                "message" => "User not found",
            ], 400);
        }

        if (Hash::check($request->old_password, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => (object) [
                    "old_password" => [
                        "Old password does not match."
                    ]
                ]
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Password updated successfully',
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Logged out',
        ]);
    }
}
