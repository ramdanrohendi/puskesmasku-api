<?php

namespace App\Http\Controllers\API\Configuration;

use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function getData(Request $request)
    {
        $only_patients = $request->query('only_patients');

        $query = User::query();

        if ($only_patients) {
            $query = $query->where('role', 'pasien');
        }
        $users = $query->orderBy('created_at', 'desc');

        return DataTables::of($users)
            ->addIndexColumn()
            ->make(true);
    }

    public function getOne(string $id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get User data!',
            'data' => $user
        ]);
    }

    public function changePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $user = User::find($id);

        if (empty($user)) {
            return response()->json([
                "status" => "error",
                "message" => "User not found",
            ], 400);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Password updated successfully',
        ]);
    }

    public function changeRole(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'role' => 'required|string|in:admin,dokter,pasien',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $user = User::find($id);

        if (empty($user)) {
            return response()->json([
                "status" => "error",
                "message" => "User not found",
            ], 400);
        }

        $user->role = $request->role;
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Role updated successfully',
        ]);
    }

    public function toggleActive($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            return response()->json([
                "status" => "error",
                "message" => "User not found",
            ], 400);
        }

        $user->is_active = !$user->is_active;
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Status updated successfully',
            'data' => [
                'is_active' => $user->is_active,
            ],
        ]);
    }

    public function destroy(string $id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $user->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'User deleted successfully',
        ]);
    }
}
