<?php

namespace App\Http\Controllers\API\Configuration;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\WebsiteSetting;
use Illuminate\Http\Request;

class WebsiteSettingController extends Controller
{
    public function getData()
    {
        $websiteSetting = WebsiteSetting::first();

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Website Setting data!',
            'data' => $websiteSetting
        ]);
    }

    public function setData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'favicon' => 'required|file|max:5120',
            'icon' => 'required|file|image|max:5120',
            'logo' => 'required|file|image|max:5120',
            'about' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $websiteSetting = WebsiteSetting::first();

        $favicon_name = '';
        try {
            if (@$request->hasFile('favicon')) {
                if (!empty($websiteSetting)) {
                    if ($websiteSetting->favicon) {
                        Storage::delete('public/configuration/website/'.$websiteSetting->favicon);
                    }
                }

                $favicon_name = "favicon_" . strtolower(str_replace(' ', '-', $request->file('favicon')->getClientOriginalName()));
                $request->file('favicon')->storeAs('public/configuration/website/', $favicon_name);
            } else {
                if (!empty($websiteSetting)) {
                    if ($websiteSetting->favicon) {
                        $favicon_name = $websiteSetting->favicon;
                    }
                } else {
                    return response()->json([
                        "status" => "error",
                        "message" => "Validation error",
                        "data" => (object) [
                            "favicon" => [
                                "The favicon field is required."
                            ]
                        ]
                    ], 400);
                }
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        $icon_name = '';
        try {
            if (@$request->hasFile('icon')) {
                if (!empty($websiteSetting)) {
                    if ($websiteSetting->icon) {
                        Storage::delete('public/configuration/website/'.$websiteSetting->icon);
                    }
                }

                $icon_name = "icon_" . strtolower(str_replace(' ', '-', $request->file('icon')->getClientOriginalName()));
                $request->file('icon')->storeAs('public/configuration/website/', $icon_name);
            } else {
                if (!empty($websiteSetting)) {
                    if ($websiteSetting->icon) {
                        $icon_name = $websiteSetting->icon;
                    }
                } else {
                    return response()->json([
                        "status" => "error",
                        "message" => "Validation error",
                        "data" => (object) [
                            "icon" => [
                                "The icon field is required."
                            ]
                        ]
                    ], 400);
                }
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        $logo_name = '';
        try {
            if (@$request->hasFile('logo')) {
                if (!empty($websiteSetting)) {
                    if ($websiteSetting->logo) {
                        Storage::delete('public/configuration/website/'.$websiteSetting->logo);
                    }
                }

                $logo_name = "logo_" . strtolower(str_replace(' ', '-', $request->file('logo')->getClientOriginalName()));
                $request->file('logo')->storeAs('public/configuration/website/', $logo_name);
            } else {
                if (!empty($websiteSetting)) {
                    if ($websiteSetting->logo) {
                        $logo_name = $websiteSetting->logo;
                    }
                } else {
                    return response()->json([
                        "status" => "error",
                        "message" => "Validation error",
                        "data" => (object) [
                            "logo" => [
                                "The logo field is required."
                            ]
                        ]
                    ], 400);
                }
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        if (!empty($websiteSetting)) {
            $websiteSetting->title = $request->title;
            $websiteSetting->favicon = $favicon_name;
            $websiteSetting->icon = $icon_name;
            $websiteSetting->logo = $logo_name;
            $websiteSetting->about = $request->about;
            $websiteSetting->save();
        } else {
            $websiteSetting = WebsiteSetting::create([
                'title' => $request->title,
                'favicon' => $favicon_name,
                'icon' => $icon_name,
                'logo' => $logo_name,
                'about' => $request->about,
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Website Setting updated successfully',
            'data' => $websiteSetting,
        ]);
    }
}
