<?php

namespace App\Http\Controllers\API\Content;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    public function getData()
    {
        $banner = Banner::first();

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Banner data!',
            'data' => $banner
        ]);
    }

    public function setData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'img' => 'nullable|file|image|max:5120',
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $banner = Banner::first();

        $img_name = '';
        try {
            if (@$request->hasFile('img')) {
                if (!empty($banner)) {
                    Storage::delete('public/content/'.$banner->img);
                }

                $img_name = "banner_" . strtolower(str_replace(' ', '-', $request->file('img')->getClientOriginalName()));
                $request->file('img')->storeAs('public/content/', $img_name);
            } else {
                if (!empty($banner)) {
                    if ($banner->img) {
                        $img_name = $banner->img;
                    }
                } else {
                    return response()->json([
                        "status" => "error",
                        "message" => "Validation error",
                        "data" => (object) [
                            "img" => [
                                "The img field is required."
                            ]
                        ]
                    ], 400);
                }
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        if (!empty($banner)) {
            $banner->img = $img_name;
            $banner->description = $request->description;
            $banner->save();
        } else {
            $banner = Banner::create([
                'img' => $img_name,
                'description' => $request->description
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Banner updated successfully',
            'data' => $banner,
        ]);
    }
}
