<?php

namespace App\Http\Controllers\API\Content;

use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Contact;

class ContactController extends Controller
{
    public function getData(Request $request)
    {
        $contacts = Contact::orderBy('created_at', 'asc');

        return DataTables::of($contacts)
            ->addIndexColumn()
            ->make(true);
    }

    public function getOne(string $id)
    {
        $contact = Contact::find($id);
        if (empty($contact)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Contact data!',
            'data' => $contact
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'icon' => 'required|file|image|max:5120',
            'description' => 'required|string',
            'redirect_link' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $icon_name = '';
        try {
            if (@$request->hasFile('icon')) {
                $icon_name = "icon_" . explode('-', Str::uuid())[4] . '_' . strtolower(str_replace(' ', '-', $request->file('icon')->getClientOriginalName()));
                $request->file('icon')->storeAs('public/content/contact/', $icon_name);
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        $contact = Contact::create([
            'icon' => $icon_name,
            'description' => $request->description,
            'redirect_link' => $request->redirect_link,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Contact created successfully',
            'data' => $contact,
        ]);
    }

    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'icon' => 'nullable|file|image|max:5120',
            'description' => 'required|string',
            'redirect_link' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $contact = Contact::find($id);
        if (empty($contact)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $icon_name = '';
        try {
            if (@$request->hasFile('icon')) {
                if (!empty($contact)) {
                    Storage::delete('public/content/contact/'.$contact->icon);
                }

                $icon_name = "icon_" . explode('-', Str::uuid())[4] . '_' . strtolower(str_replace(' ', '-', $request->file('icon')->getClientOriginalName()));
                $request->file('icon')->storeAs('public/content/contact/', $icon_name);
            } else {
                if ($contact->icon) {
                    $icon_name = $contact->icon;
                }
            }
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                "status" => "error",
                "message" => "Uploading file failed!",
                "data" => $th
            ], 500);
        }

        $contact->icon = $icon_name;
        $contact->description = $request->description;
        $contact->redirect_link = $request->redirect_link;
        $contact->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Contact updated successfully',
            'data' => $contact,
        ]);
    }

    public function destroy(string $id)
    {
        $contact = Contact::find($id);
        if (empty($contact)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        if (!empty($contact->icon)) {
            Storage::delete('public/content/contact/'.$contact->icon);
        }

        $contact->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Contact deleted successfully',
        ]);
    }
}
