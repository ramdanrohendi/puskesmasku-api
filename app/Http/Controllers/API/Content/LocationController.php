<?php

namespace App\Http\Controllers\API\Content;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Location;

class LocationController extends Controller
{
    public function getData()
    {
        $location = Location::first();

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Location data!',
            'data' => $location
        ]);
    }

    public function setData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required|decimal:1,6',
            'longitude' => 'required|decimal:1,6',
            'zoom' => 'nullable|numeric',
            'label' => 'nullable|string',
            'full_address' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $location = Location::first();

        if (!empty($location)) {
            $location->latitude = $request->latitude;
            $location->longitude = $request->longitude;
            $location->zoom = $request->zoom;
            $location->label = $request->label;
            $location->full_address = $request->full_address;
            $location->save();
        } else {
            $location = Location::create([
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'zoom' => $request->zoom,
                'label' => $request->label,
                'full_address' => $request->full_address,
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Location updated successfully',
            'data' => $location,
        ]);
    }
}
