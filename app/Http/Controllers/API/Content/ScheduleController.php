<?php

namespace App\Http\Controllers\API\Content;

use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Schedule;

class ScheduleController extends Controller
{
    public function getData(Request $request)
    {
        $schedules = Schedule::orderBy('position', 'asc');

        return DataTables::of($schedules)
            ->addIndexColumn()
            ->rawColumns(['day'])
            ->make(true);
    }

    public function getOne(string $id)
    {
        $schedule = Schedule::find($id);
        if (empty($schedule)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Schedule data!',
            'data' => $schedule
        ]);
    }

    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|string|in:primary,warning,danger',
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $schedule = Schedule::find($id);
        if (empty($schedule)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $schedule->status = $request->status;
        $schedule->description = $request->description;
        $schedule->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Schedule updated successfully',
            'data' => $schedule,
        ]);
    }
}
