<?php

namespace App\Http\Controllers\API\Content;

use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;

class ServiceController extends Controller
{
    public function getData(Request $request)
    {
        $services = Service::orderBy('position', 'asc');

        return DataTables::of($services)
            ->addIndexColumn()
            ->make(true);
    }

    public function getOne(string $id)
    {
        $service = Service::find($id);
        if (empty($service)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Service data!',
            'data' => $service
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string',
            'redirect_link' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $position = 0;
        $last_service = Service::select('position')->orderBy('position', 'desc')->first();
        if (empty($last_service)) {
            $position = 1;
        } else {
            $position = $last_service->position + 1;
        }

        $service = Service::create([
            'title' => $request->title,
            'description' => $request->description,
            'redirect_link' => $request->redirect_link,
            'position' => $position,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Service created successfully',
            'data' => $service,
        ]);
    }

    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string',
            'redirect_link' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $service = Service::find($id);
        if (empty($service)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $service->title = $request->title;
        $service->description = $request->description;
        $service->redirect_link = $request->redirect_link;
        $service->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Service updated successfully',
            'data' => $service,
        ]);
    }

    public function updatePosition(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'direction' => 'required|string|in:up,down',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $direction = $request->direction;

        $service = Service::find($id);
        if (empty($service)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $other_position = $service->position;
        if ($direction == 'up') {
            $other_position--;
        }

        if ($direction == 'down') {
            $other_position++;
        }

        $other_service = Service::where('position', $other_position)->first();
        if (empty($other_service)) {
            return response()->json([
                "status" => "error",
                "message" => "The position is not available!",
            ], 400);
        }

        $other_service->position = $service->position;
        $other_service->save();

        $service->position = $other_position;
        $service->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Service update position successfully',
            'data' => $service,
        ]);
    }

    public function destroy(string $id)
    {
        $service = Service::find($id);
        if (empty($service)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $service_down_this = Service::where('position', '>', $service->position)->get();
        if (!empty($service_down_this) || count($service_down_this) > 0) {
            foreach ($service_down_this as $data) {
                $data->position = $data->position - 1;
                $data->save();
            }
        }

        $service->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Service deleted successfully',
        ]);
    }
}
