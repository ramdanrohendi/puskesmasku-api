<?php

namespace App\Http\Controllers\API\Content;

use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShowDoctor;
use App\Models\User;

class ShowDoctorController extends Controller
{
    public function getData(Request $request)
    {
        $doctors = ShowDoctor::with('user')->orderBy('position', 'asc');

        return DataTables::of($doctors)
            ->addIndexColumn()
            ->make(true);
    }

    public function getOne(string $id)
    {
        $doctor = ShowDoctor::with('user')->find($id);
        if (empty($doctor)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Doctor data!',
            'data' => $doctor
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $user = User::find($request->user_id);
        if (empty($user) || !$user->hasRole(['dokter'])) {
            return response()->json([
                "status" => "error",
                "message" => "Data doctor not found.",
            ], 400);
        }

        $is_exist = ShowDoctor::where('user_id', $request->user_id)->first();
        if (!empty($is_exist)) {
            return response()->json([
                "status" => "error",
                "message" => "Doctor data has been added.",
            ], 400);
        }

        $position = 0;
        $last_doctor = ShowDoctor::select('position')->orderBy('position', 'desc')->first();
        if (empty($last_doctor)) {
            $position = 1;
        } else {
            $position = $last_doctor->position + 1;
        }

        $doctor = ShowDoctor::create([
            'user_id' => $request->user_id,
            'position' => $position,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Doctor added successfully',
            'data' => $doctor,
        ]);
    }

    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $user = User::find($request->user_id);
        if (empty($user) || !$user->hasRole(['dokter'])) {
            return response()->json([
                "status" => "error",
                "message" => "Data doctor not found.",
            ], 400);
        }

        $doctor = ShowDoctor::find($id);
        if (empty($doctor)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $is_exist = ShowDoctor::where('user_id', $request->user_id)->first();
        if (!empty($is_exist)) {
            return response()->json([
                "status" => "error",
                "message" => "Doctor data has been added.",
            ], 400);
        }

        $doctor->user_id = $request->user_id;
        $doctor->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Show doctor updated successfully',
            'data' => $doctor,
        ]);
    }

    public function updatePosition(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'direction' => 'required|string|in:up,down',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $direction = $request->direction;

        $doctor = ShowDoctor::find($id);
        if (empty($doctor)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $other_position = $doctor->position;
        if ($direction == 'up') {
            $other_position--;
        }

        if ($direction == 'down') {
            $other_position++;
        }

        $other_doctor = ShowDoctor::where('position', $other_position)->first();
        if (empty($other_doctor)) {
            return response()->json([
                "status" => "error",
                "message" => "The position is not available!",
            ], 400);
        }

        $other_doctor->position = $doctor->position;
        $other_doctor->save();

        $doctor->position = $other_position;
        $doctor->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Show doctor update position successfully',
            'data' => $doctor,
        ]);
    }

    public function destroy(string $id)
    {
        $doctor = ShowDoctor::find($id);
        if (empty($doctor)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $doctor_down_this = ShowDoctor::where('position', '>', $doctor->position)->get();
        if (!empty($doctor_down_this) || count($doctor_down_this) > 0) {
            foreach ($doctor_down_this as $data) {
                $data->position = $data->position - 1;
                $data->save();
            }
        }

        $doctor->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Show doctor deleted successfully',
        ]);
    }
}
