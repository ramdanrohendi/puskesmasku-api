<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use App\Models\MedicalHistory;
use Illuminate\Http\Request;
use App\Models\User;

class MedicalHistoryController extends Controller
{
    public function getData(Request $request, string $mode = 'datatable')
    {
        if ($mode == 'manual') {
            $search = $request->query('search');
            $limit = $request->query('limit');
            $page = $request->query('page');
            $order = $request->query('order') ?? 'DESC';
            $patient_id = $request->query('patient_id');
            if ($request->user()->hasRole(['pasien'])) {
                $patient_id = $request->user()->id;
            }

            try {
                $query = MedicalHistory::query();

                if ($patient_id) {
                    $query = $query->where('patient_id', $patient_id);
                }

                if ($search) {
                    $query = $query->where('complaint', 'like', "%" . $search . "%")
                                    ->orWhere('physical_examination_results', 'like', "%" . $search . "%")
                                    ->orWhere('diagnosis', 'like', "%" . $search . "%")
                                    ->orWhere('medicine', 'like', "%" . $search . "%")
                                    ->orWhere('notes', 'like', "%" . $search . "%")
                                    ->orWhere('created_at', 'like', "%" . $search . "%");
                }

                $data = $query->orderBy('created_at', $order)->offset($page)->paginate($limit);

                return response()->json([
                    "status" => "success",
                    "message" => "OK",
                    "data" => $data
                ]);
            } catch (\Throwable $th) {
                report($th);
                return response()->json([
                    "status" => "error",
                    "message" => "Internal Server Error!",
                    "data" => $th
                ], 500);
            }
        }

        $patient_id = $request->query('patient_id');
        if ($request->user()->hasRole(['pasien'])) {
            $patient_id = $request->user()->id;
        }

        $medicalHistories = MedicalHistory::orderBy('created_at', 'desc');

        if (!empty($patient_id)) {
            $medicalHistories = $medicalHistories->where('patient_id', $patient_id);
        }

        return DataTables::of($medicalHistories)
            ->addIndexColumn()
            ->make(true);
    }

    public function getOne(string $id)
    {
        $medicalHistory = MedicalHistory::find($id);
        if (empty($medicalHistory)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Succesfully get Medical History data!',
            'data' => $medicalHistory
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'patient_id' => 'required|string',
            'complaint' => 'nullable|string',
            'physical_examination_results' => 'nullable|string',
            'diagnosis' => 'nullable|string',
            'medicine' => 'nullable|string',
            'notes' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $user = User::find($request->patient_id);
        if (empty($user) || !$user->hasRole(['pasien'])) {
            return response()->json([
                "status" => "error",
                "message" => "Data patient not found.",
            ], 400);
        }

        $medicalHistory = MedicalHistory::create([
            'patient_id' => $request->patient_id,
            'doctor_id' => $request->user()->id,
            'complaint' => $request->complaint,
            'physical_examination_results' => $request->physical_examination_results,
            'diagnosis' => $request->diagnosis,
            'medicine' => $request->medicine,
            'notes' => $request->notes,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Medical history created successfully',
            'data' => $medicalHistory,
        ]);
    }

    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'complaint' => 'nullable|string',
            'physical_examination_results' => 'nullable|string',
            'diagnosis' => 'nullable|string',
            'medicine' => 'nullable|string',
            'notes' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "error",
                "message" => "Validation error",
                "data" => $validator->errors()
            ], 400);
        }

        $medicalHistory = MedicalHistory::find($id);
        if (empty($medicalHistory)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $medicalHistory->complaint = $request->complaint;
        $medicalHistory->physical_examination_results = $request->physical_examination_results;
        $medicalHistory->diagnosis = $request->diagnosis;
        $medicalHistory->medicine = $request->medicine;
        $medicalHistory->notes = $request->notes;
        $medicalHistory->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Medical history updated successfully',
            'data' => $medicalHistory,
        ]);
    }

    public function destroy(string $id)
    {
        $medicalHistory = MedicalHistory::find($id);
        if (empty($medicalHistory)) {
            return response()->json([
                "status" => "error",
                "message" => "Data not found.",
            ], 400);
        }

        $medicalHistory->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Medical history deleted successfully',
        ]);
    }
}
