<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class JwtAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        try {
            // $verify = JWTAuth::parseToken()->authenticate();
            // dd($request->header());

            $token = explode(" ", $request->header("authorization"))[1];
            $authenticate = JWTAuth::setToken($token)->authenticate();
        } catch (Exception $e) {
            report($e);
            if ($e instanceof \PHPOpenSourceSaver\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['status' => 'error', 'message' => 'Token is Invalid'], 401);
            } else if ($e instanceof \PHPOpenSourceSaver\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['status' => 'error', 'message' => 'Token is Expired'], 401);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Forbidden access!'], 403);
            }
        }

        return $next($request);
    }
}
