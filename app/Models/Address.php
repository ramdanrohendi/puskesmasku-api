<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Address extends Model
{
    use HasFactory, Uuid;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'profile_id',
        'detail',
        'rt',
        'rw',
        'village', //Kelurahan
        'district', //Kecamatan
        'city',
        'province',
        'postal_code',
        'country',
    ];

    public function profile(): BelongsTo
    {
        return $this->belongsTo(Profile::class);
    }
}
