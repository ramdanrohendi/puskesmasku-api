<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Banner extends Model
{
    use HasFactory, Uuid;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'img',
        'description',
    ];

    protected $appends = [
        'img_path',
    ];

    public function getImgPathAttribute()
    {
        $path = url('/') . '/storage/content/'.$this->img;

        return $path;
    }

}
