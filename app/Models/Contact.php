<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Contact extends Model
{
    use HasFactory, Uuid;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'icon',
        'description',
        'redirect_link',
    ];

    protected $appends = [
        'icon_path',
    ];

    public function getIconPathAttribute()
    {
        $path = url('/') . '/storage/content/contact/'.$this->icon;

        return $path;
    }
}
