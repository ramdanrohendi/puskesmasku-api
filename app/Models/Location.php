<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Location extends Model
{
    use HasFactory, Uuid;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'latitude',
        'longitude',
        'zoom',
        'label',
        'full_address',
    ];
}
