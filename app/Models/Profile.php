<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Profile extends Model
{
    use HasFactory, Uuid;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'user_id',
        'photo',
        'nik',
        'first_name',
        'last_name',
        'birth_place',
        'birth_date',
        'gender',
        'religion',
        'marital_status',
        'no_bpjs',
        'about_me',
    ];

    protected $appends = [
        'photo_path',
    ];

    protected $with = [
        'address',
    ];

    public function getPhotoPathAttribute()
    {
        $path = url('/') . '/assets/img/default-profile.png';

        if ($this->photo) {
            $path = url('/') . '/storage/profile/'.$this->nik.'/'. $this->photo;
        }

        return $path;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function address(): HasOne
    {
        return $this->hasOne(Address::class);
    }
}
