<?php

namespace App\Models;

use App\Traits\Uuid;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, Uuid;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'email',
        'password',
        'role',
        'is_active'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'password' => 'hashed',
    ];

    protected $with = [
        'profile',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function hasRole($role)
    {
        return in_array($this->role, $role);
    }

    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class);
    }

    public function show_doctors(): HasOne
    {
        return $this->hasOne(ShowDoctor::class);
    }

    public function my_medical_histories(): HasMany
    {
        return $this->hasMany(MedicalHistory::class, 'patient_id');
    }

    public function medical_histories_checked(): HasMany
    {
        return $this->hasMany(MedicalHistory::class, 'doctor_id');
    }
}
