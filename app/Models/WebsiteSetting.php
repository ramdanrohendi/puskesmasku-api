<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class WebsiteSetting extends Model
{
    use HasFactory, Uuid;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'title',
        'favicon',
        'icon',
        'logo',
        'about',
    ];

    protected $appends = [
        'favicon_path',
        'icon_path',
        'logo_path',
    ];

    public function getFaviconPathAttribute()
    {
        $path = url('/') . '/storage/configuration/website/'.$this->favicon;

        return $path;
    }

    public function getIconPathAttribute()
    {
        $path = url('/') . '/storage/configuration/website/'.$this->icon;

        return $path;
    }

    public function getLogoPathAttribute()
    {
        $path = url('/') . '/storage/configuration/website/'.$this->logo;

        return $path;
    }
}
