<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('user_id')->references('id')->on('users')->constrained()->cascadeOnDelete();
            $table->string('photo')->nullable();
            $table->char('nik', 16)->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('birth_place');
            $table->date('birth_date');
            $table->char('gender', 1);
            $table->string('religion');
            $table->string('marital_status');
            $table->string('no_bpjs')->nullable();
            $table->text('about_me')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('profiles');
    }
};
