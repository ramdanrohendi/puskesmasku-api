<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('profile_id')->references('id')->on('profiles')->constrained()->cascadeOnDelete();
            $table->text('detail');
            $table->char('rt', 3);
            $table->char('rw', 3);
            $table->string('village'); //Kelurahan
            $table->string('district'); //Kecamatan
            $table->string('city');
            $table->string('province');
            $table->char('postal_code', 6);
            $table->string('country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('addresses');
    }
};
