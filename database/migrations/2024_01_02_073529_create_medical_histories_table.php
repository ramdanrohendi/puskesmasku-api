<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('medical_histories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('patient_id')->references('id')->on('users')->constrained()->cascadeOnDelete();
            $table->foreignUuid('doctor_id')->references('id')->on('users')->constrained()->cascadeOnDelete();
            $table->text('complaint')->nullable();
            $table->text('physical_examination_results')->nullable();
            $table->text('diagnosis')->nullable();
            $table->text('medicine')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medical_histories');
    }
};
