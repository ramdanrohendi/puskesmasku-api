<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Schedule;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = $this->dataSchedule();

        foreach ($data as $value) {
            Schedule::updateOrCreate([
                'day' => $value['day'],
            ], [
                'status' => @$value['status'] ?? 'primary',
                'position' => $value['position'],
                'description' => $value['description'],
            ]);
        }
    }

    private function dataSchedule()
    {
        $data = [
            [
                'position' => 1,
                'day' => 'Senin',
                'description' => '08.00 - 17.00 WIB',
            ],
            [
                'position' => 2,
                'day' => 'Selasa',
                'description' => '08.00 - 17.00 WIB',
            ],
            [
                'position' => 3,
                'day' => 'Rabu',
                'description' => '08.00 - 17.00 WIB',
            ],
            [
                'position' => 4,
                'day' => 'Kamis',
                'description' => '08.00 - 17.00 WIB',
            ],
            [
                'position' => 5,
                'day' => 'Jum\'at',
                'description' => '08.00 - 17.00 WIB',
            ],
            [
                'position' => 6,
                'day' => 'Sabtu',
                'status' => 'warning',
                'description' => '08.00 - 12.00 WIB',
            ],
            [
                'position' => 7,
                'day' => 'Minggu',
                'status' => 'danger',
                'description' => '(Tutup)',
            ],
        ];

        return $data;
    }
}
