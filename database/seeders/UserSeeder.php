<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\Profile;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = $this->dataUser();

        foreach ($data as $value) {
            $user = User::updateOrCreate([
                'email' => $value['email'],
            ], [
                'password' => $value['password'],
                'role' => $value['role'],
            ]);

            Profile::updateOrCreate([
                'user_id' => $user->id,
            ], [
                'nik' => $value['profile']['nik'],
                'first_name' => $value['profile']['first_name'],
                'last_name' => $value['profile']['last_name'],
                'birth_place' => $value['profile']['birth_place'],
                'birth_date' => $value['profile']['birth_date'],
                'gender' => $value['profile']['gender'],
                'religion' => $value['profile']['religion'],
                'marital_status' => $value['profile']['marital_status'],
            ]);
        }
    }

    private function dataUser()
    {
        $data = [
            [
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin12345'),
                'role' => 'admin',
                'profile' => [
                    'nik' => '000000000000000A',
                    'first_name' => 'Admin',
                    'last_name' => 'Puskesmasku',
                    'birth_place' => 'Bumi',
                    'birth_date' => '1000-01-01',
                    'gender' => 'L',
                    'religion' => 'Islam',
                    'marital_status' => '-',
                ],
            ],
            [
                'email' => 'dokter@gmail.com',
                'password' => Hash::make('dokter12345'),
                'role' => 'dokter',
                'profile' => [
                    'nik' => '000000000000000B',
                    'first_name' => 'Dokter',
                    'last_name' => 'Puskesmasku',
                    'birth_place' => 'Bumi',
                    'birth_date' => '1000-01-01',
                    'gender' => 'L',
                    'religion' => 'Islam',
                    'marital_status' => '-',
                ],
            ],
            [
                'email' => 'doktertest@gmail.com',
                'password' => Hash::make('dokter12345'),
                'role' => 'dokter',
                'profile' => [
                    'nik' => '000000000000001B',
                    'first_name' => 'Dokter',
                    'last_name' => 'Puskesmasku',
                    'birth_place' => 'Bumi',
                    'birth_date' => '1000-01-01',
                    'gender' => 'L',
                    'religion' => 'Islam',
                    'marital_status' => '-',
                ],
            ],
            [
                'email' => 'pasien@gmail.com',
                'password' => Hash::make('pasien12345'),
                'role' => 'pasien',
                'profile' => [
                    'nik' => '000000000000000C',
                    'first_name' => 'Pasien',
                    'last_name' => 'Puskesmasku',
                    'birth_place' => 'Bumi',
                    'birth_date' => '1000-01-01',
                    'gender' => 'L',
                    'religion' => 'Islam',
                    'marital_status' => '-',
                ],
            ],
            [
                'email' => 'pasientest@gmail.com',
                'password' => Hash::make('pasien12345'),
                'role' => 'pasien',
                'profile' => [
                    'nik' => '000000000000001C',
                    'first_name' => 'Pasien',
                    'last_name' => 'Test Puskesmasku',
                    'birth_place' => 'Bumi',
                    'birth_date' => '1000-01-01',
                    'gender' => 'L',
                    'religion' => 'Islam',
                    'marital_status' => '-',
                ],
            ],
        ];

        return $data;
    }
}
