<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\MedicalHistoryController;
use App\Http\Controllers\API\Content\{
    BannerController,
    ServiceController,
    ShowDoctorController,
    ScheduleController,
    LocationController,
    ContactController,
};
use App\Http\Controllers\API\Configuration\{
    WebsiteSettingController,
    UserController,
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/', function ()  {
    return response()->json([
        "status" => "success",
        "message" => "API version 1.0.0"
    ]);
});

Route::get('/admin', function ()  {
    return response()->json([
        "status" => "success admin",
        "message" => "API version 1.0.0",
        "data" => auth()->user()
    ]);
})->middleware(['jwt.verify', 'role:admin']);

// Auth Route
Route::controller(AuthController::class)->group(function() {
    Route::post('/login', 'login');
    Route::post('/register', 'register');
    Route::post('/logout', 'logout')->middleware('jwt.verify');

    Route::middleware(['jwt.verify', 'mine'])->group(function() {
        Route::post('/update-profile/{id}', 'updateProfile');
        Route::post('/change-password/{id}', 'changePassword');
    });
});

// Medical History Route
Route::prefix('medical-history')->controller(MedicalHistoryController::class)->group(function() {
    Route::middleware(['jwt.verify'])->group(function() {
        Route::get('/', 'getData');
        Route::get('/{mode?}/get', 'getData');
        Route::get('/{id}', 'getOne');
    });

    Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
        Route::post('/', 'store');
        Route::post('/{id}', 'update');
        Route::delete('/{id}', 'destroy');
    });
});

// Content Route
Route::prefix('content')->group(function () {
    Route::get('/', function ()  {
        return response()->json([
            "status" => "success",
            "message" => "This is Content API",
        ]);
    });

    Route::prefix('banner')->controller(BannerController::class)->group(function() {
        Route::get('/', 'getData');

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::post('/', 'setData');
        });
    });

    Route::prefix('service')->controller(ServiceController::class)->group(function() {
        Route::get('/', 'getData');
        Route::get('/{id}', 'getOne');

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::post('/', 'store');
            Route::post('/{id}', 'update');
            Route::post('/{id}/update-position', 'updatePosition');
            Route::delete('/{id}', 'destroy');
        });
    });

    Route::prefix('show-doctor')->controller(ShowDoctorController::class)->group(function() {
        Route::get('/', 'getData');
        Route::get('/{id}', 'getOne');

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::post('/', 'store');
            Route::post('/{id}', 'update');
            Route::post('/{id}/update-position', 'updatePosition');
            Route::delete('/{id}', 'destroy');
        });
    });

    Route::prefix('schedule')->controller(ScheduleController::class)->group(function() {
        Route::get('/', 'getData');
        Route::get('/{id}', 'getOne');

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::post('/{id}', 'update');
        });
    });

    Route::prefix('location')->controller(LocationController::class)->group(function() {
        Route::get('/', 'getData');

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::post('/', 'setData');
        });
    });

    Route::prefix('contact')->controller(ContactController::class)->group(function() {
        Route::get('/', 'getData');
        Route::get('/{id}', 'getOne');

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::post('/', 'store');
            Route::post('/{id}', 'update');
            Route::delete('/{id}', 'destroy');
        });
    });
});

// Setting Admin Route
Route::prefix('configuration')->group(function () {
    Route::get('/', function ()  {
        return response()->json([
            "status" => "success",
            "message" => "This is Configuration API",
        ]);
    });

    Route::prefix('website-setting')->controller(WebsiteSettingController::class)->group(function() {
        Route::get('/', 'getData');

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::post('/', 'setData');
        });
    });

    Route::prefix('user')->controller(UserController::class)->group(function() {
        Route::get('/{id}', 'getOne')->middleware(['jwt.verify', 'mine']);

        Route::middleware(['jwt.verify', 'role:admin'])->group(function() {
            Route::get('/', 'getData');
            Route::get('/{id}/toggle-active', 'toggleActive');
            Route::post('/{id}/change-password', 'changePassword');
            Route::post('/{id}/change-role', 'changeRole');
            Route::delete('/{id}', 'destroy');
        });
    });
});
